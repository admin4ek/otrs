#!/usr/bin/python2.7
'''
OTRS tickets.
It should get needed info and put it to L3 Status page.
'''
import json
import urllib2
import MySQLdb
import re
from datetime import datetime, timedelta
__version__ = '3.0.1-stable'
__date__ = '19 january 2016'
__author__ = 'Oleksandr Tretiak'

# set data for brands

uk_otrs = {"url": "http://otrs.uk.chimera.uk2group.com:8185/",
           "databases": ['uk2newt', 'uk2needupt', 'uk2currt'],
           "ticket_brand_link": 'https://tickets.uk2.net/otrs/index.pl?Action=AgentTicketZoom;TicketID=',
           "queues": ['UK2.net - VPS Support']}

us_otrs = {"url": "http://otrs.us.chimera.uk2group.com:8185/",
           "databases": ['midnewt', 'midneedupt', 'midcurrt'],
           "ticket_brand_link": 'https://tickets.uk2group.com/otrs/index.pl?Action=AgentTicketZoom;TicketID=',
           "queues": ['MP - Cloud Support - L1', 'MP - Cloud Support L2', 'MP - Dedicated Server']}

brands = {
          'UK2.net': uk_otrs,
          'Midphase': us_otrs}
# Database details
db_host = '109.123.121.21'
db_user = 'statuser'
db_passwd = 'mft4VJ5ZbPE7jPMX'
db_database = 'statdb'


def get_tickets(url, brand, brand_queue, databases, ticket_brand_link):
    '''
    Get ticket from the queues and sort.
    Clear tables in DB
    '''
    # connect to the url and get tickets from the brand
    req = urllib2.Request(url)
    opener = urllib2.build_opener()
    f = opener.open(req)
    tickets = {}
    # f = unicode(opener.open(req), "UTF-8")
    otrs_check_error = ''
    try:
        tickets = json.loads(f.read())
        # print tickets
    except Exception, Err:
        print Err
        otrs_check_error = 'OTRS API error'
    # print tickets
    # clear Database
    monitor_db = MySQLdb.connect(host=db_host, user=db_user, passwd=db_passwd, db=db_database)
    cur = monitor_db.cursor()
    error_print = ""
    for tables in databases:
        clear_db = ("truncate {tables}".format(tables=tables))
        if len(otrs_check_error) > 0:
            error_print = """INSERT INTO {table} (status) VALUES ('{data}');""".format(table=tables, data=otrs_check_error)
        try:
            cur.execute(clear_db)
            if len(error_print) > 0:
                cur.execute(error_print)
        except Exception, Err:
            print Err
    monitor_db.commit()
    # create the dictionaty for the tickets with the status open
    monitor_tickets = {}
    for ticket in tickets:
        # get tickets with the status "open" and check if it in the brand queues
        if (ticket['status'] == 'open' or ticket['status'] == 'new') and (ticket['queue_name'] in brand_queue):
            # get ticekt info from the queues
            ticket_status_q = ticket['status']
            print ticket_status_q
            ticket_id = int(ticket['ticket_number'])
            print ticket_id
            id = int(ticket['ticket_number'])
            # ticket_status = ticket['status']
            ticket_updates = int(ticket['updates'])
            # print ticket_id, ticket_updates
            try:
                ticket_title = str(ticket['title'])
            except Exception, Err:
                print Err
                ticket_title = 'encoding error API'
            ticket_title = re.escape(ticket_title)
            ticket_priority = str(ticket['ticket_priority'])
            ticket_id_link = int(ticket['ticket_id'])
            ticket_queue = str(ticket['queue_name'])
            ticket_owner = str(ticket['owned_by'])
            ticket_owner = re.escape(ticket_owner)
            ticket_last_reply_from = str(ticket['last_changed_by'])
            ticket_last_reply_from = re.escape(ticket_last_reply_from)
            ticket_change_time_ts = datetime.fromtimestamp(int(ticket['change_time_ts']))
            ticket_link = "{ticket_brand_link}{id}".format(ticket_brand_link=ticket_brand_link, id=ticket_id_link)
            # check last update time in the ticket
            status, ticket_diff_time, table = diff_time(ticket_change_time_ts, ticket_updates, databases, ticket_status_q)
            # DB structure
            # id | ticket_number | last_changed_by | owned_by | title | status | change_time | queue_name | ticket_brand_link
            monitor_tickets.setdefault(id, []).append(ticket_last_reply_from)
            monitor_tickets.setdefault(id, []).append(ticket_owner)
            monitor_tickets.setdefault(id, []).append(ticket_title)
            monitor_tickets.setdefault(id, []).append(ticket_priority)
            monitor_tickets.setdefault(id, []).append(ticket_diff_time)
            monitor_tickets.setdefault(id, []).append(ticket_queue)
            monitor_tickets.setdefault(id, []).append(ticket_link)
            monitor_tickets.setdefault(id, []).append(status)
            monitor_tickets.setdefault(id, []).append(table)
    gen_insert(monitor_tickets)


def diff_time(ticket_change_time_ts, ticket_updates, databases, ticket_status_q):
    # check time frame and put the result back
    # time_now = time.time()
    time_now = datetime.now()
    diff = time_now - ticket_change_time_ts
    # print ticket_updates
    # print diffB
    if ticket_status_q == 'new':
        status = 'new'
        table = databases[0]
    else:
        if (diff > timedelta(minutes=15)) and (ticket_updates == 1):
            status = 'unupdated'
            table = databases[1]
        if (diff > timedelta(minutes=30)) and (ticket_updates == 2):
                status = 'unupdated'
                table = databases[1]
        if (diff > timedelta(minutes=60)) and (ticket_updates > 3):
                status = 'unupdated'
                table = databases[1]
        else:
                status = 'current'
                table = databases[2]
    # print diff, status, table
    ticket_diff_time = "{diff}".format(diff=str(diff).split('.')[0])
    return(status, ticket_diff_time, table)


def gen_insert(monitor_tickets):
    '''
    Generate insert and put data
    '''
    monitor_db = MySQLdb.connect(host=db_host, user=db_user, passwd=db_passwd, db=db_database)
    cur = monitor_db.cursor()
    for key, value in monitor_tickets.items():
        table = value[8]
        # DB structure
        # id | ticket_number | last_changed_by | owned_by | title | status | change_time | queue_name | ticket_brand_link
        monitoring_data = "{}, '{}', '{}', '{}', '{}', '{}', '{}', '{}'".format(key, value[0], value[1], value[2], value[3], value[4], value[5], value[6])
        insert = """INSERT INTO {table} (ticket_number, last_changed_by, owned_by, title, status, change_time, queue_name, ticket_link) VALUES ({data});""".format(table=table, data=monitoring_data)
        # db_insert
        try:
            cur.execute(insert)
        except Exception, Err:
            print Err
    monitor_db.commit()


def main():
    '''
    get urls, brand and queue_name form the list
    :brand: get brand form the dictionaty
    :url: link to the OTRS API
    :brand_queue: a list of the brand queues
    '''
    # set variables for the queues
    for brand, info in brands.items():
        url = info['url']

        brand_queue = info['queues']
        ticket_brand_link = info['ticket_brand_link']
        databases = info['databases']
        get_tickets(url, brand, brand_queue, databases, ticket_brand_link)

    print "All data has been uploaded"

main()
